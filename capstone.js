class Product {
    constructor(name,price){
        if(typeof name !== "string") return console.log("Error - name must be string")
        if(typeof price !== "number") return console.log("Error - price must be number")
        this.name = name;
        this.price = price;
        this.isActive = true;
    }

    archiveProduct(){
        if(!this.isActive) return "Error - product is already inactive"
        this.isActive = false
        return this
    }

    unarchiveProduct(){
        if(this.isActive) return "Error - product is already active"
        this.isActive = true
        return this
    }

    updatePrice(newPrice){
        if(typeof newPrice !== "number") return "Error - pice must be a number"
        this.price = newPrice
        return this
    }
}

class Cart{
    constructor(){
        this.contents = [];
        this.totalAmount = undefined;
    }
    
    addToCart(product,quantity){
        if(typeof product !== "object") return "Error - product must be an object"
        if(typeof quantity !== "number") return "Error - quantity must be a number"
        if(Object.getPrototypeOf(product).constructor === Product){
            this.contents.push({
                product: product,
                quantity: quantity
            })
            this.computeTotal()
            return this
        }
        else return "Error - type error, product is not of Product Class"
    }

    showCartContents(){
        this.contents.forEach(product => console.log(product))
        return this
    }

    updateProductQuantity(name, newQuantity){
        this.contents.find(product => product.product.name === name).quantity = newQuantity
        return this
    }

    computeTotal(){
        if(this.contents.length == 0) return "Cart is empty"
        let total = 0
        this.contents.forEach(product => {
            total += product.quantity*product.product.price
        })
        this.totalAmount = total
        return this
    }
    
    clearCartContents(){
        this.totalAmount = 0
        this.contents = []
        return this
    }
}

class Customer{
    constructor(email){
        this.email = email
        this.cart = new Cart()
        this.orders = {
            products:[],
            totalAmount:0
        }
    }
    
    checkOut(){
        this.cart.contents.forEach(product => this.orders.products.push(product))
        this.orders.totalAmount += this.cart.totalAmount
        this.cart.clearCartContents()
        return this
    }
}

const john = new Customer('john@mail.com')

const prodA = new Product('soap', 9.99)
const prodB = new Product('shampoo', 12.99)
const prodC = new Product('toothbrush', 4.99)
const prodD = new Product('toothpaste', 14.99)